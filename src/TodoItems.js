import React, {Component} from "react";
import FlipMove from 'react-flip-move';

class TodoItems extends Component{
    constructor(props){
        super(props);
        this.crateTasks=this.crateTasks.bind(this);
    }
    delete(key){
        this.props.delete(key);
    }
    crateTasks(item){
        return <li onClick={()=>this.delete(item.key)} key={item.key}>{item.text}</li>
    }

    render(){
        let todoEntries = this.props.entries;
        let listItems = todoEntries.map(this.crateTasks);
        console.log(this.props.entries);
        return(<ul className="theList"><FlipMove duration={500} easing="easy-out">{listItems}
               </FlipMove></ul>
              );
    }
};
export default TodoItems;
